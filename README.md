# FREEDOM! 

Streamlined Compliance with GitLab  
Zero to Hero Roadshow  

In this training session, participants will learn how to:

- Quickly set up a build pipeline
- Add automated tests and reporting
- Set up security scanning
- Generate a Software Bill of Materials (SBOM) for both application and base container code
- Automate releases and evidence generation
- Enforce security policy through compliance rules

Please review the [issues](../../issues) for step-by-step instructions.